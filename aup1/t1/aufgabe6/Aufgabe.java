import java.util.Scanner;

class Aufgabe {
	public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.print("Bitte geben Sie die 1. Zahl ein: ");
        int input1 = myScanner.nextInt();

        System.out.print("Bitte geben Sie die 2. Zahl ein: ");
        int input2 = myScanner.nextInt();

        System.out.print("Bitte geben Sie die 3. Zahl ein: ");
        int input3 = myScanner.nextInt();

        double average = (double) (input1 + input2 + input3) / 3;

        System.out.println("Der Mittelwert dieser drei Zahlen ist " + average);
	}
}