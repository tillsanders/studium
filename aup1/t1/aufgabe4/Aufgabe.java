import java.util.Scanner;
import java.util.Locale;

class Aufgabe {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Teilaufgabe a)
		System.out.print("a) Welchen Wert hat der Ausdruck '(byte) 258'? ");
		int input1 = myScanner.nextInt();
		if (input1 == (byte) 258) {
			System.out.println("Korrekt!");
		} else {
			System.out.println("Leider falsch! Die richtige Antwort ist " + (byte) 258);
		}
		System.out.println("Ein Byte hat einen Wertebereich von 2^8 = 256. Wird die Dezimalzahl 258 mittels expliziter Typumwandlung in ein Byte umgewandelt, findet ein Speicherüberlauf statt, wodurch das Ergebnis 2 ist.\n");

		// Teilaufgabe b)
		System.out.print("b) Welchen Wert hat der Ausdruck '(int) 13.75f'? ");
		int input2 = myScanner.nextInt();
		if (input2 == (int) 13.75f) {
			System.out.println("Korrekt!");
		} else {
			System.out.println("Leider falsch! Die richtige Antwort ist " + (int) 13.75f);
		}
		System.out.println("Der Wert 13.75f ist vom Typ float. Bei der expliziten Typumwandlung werden die Nachkommastellen gestrichen.\n");

		// Teilaufgabe c)
		System.out.print("c) Welchen booleeschen Wert (true oder false) hat der Ausdruck '(5 < 2) | (6 % 4 == 2)'? ");
		boolean input3 = myScanner.nextBoolean();
		if (input3 == ((5 < 2) | (6 % 4 == 2))) {
			System.out.println("Korrekt!");
		} else {
			System.out.println("Leider falsch! Die richtige Antwort ist " + ((5 < 2) | (6 % 4 == 2)));
		}
		System.out.println("Der linke Teil des Ausdrucks entspricht einem Boolean vom Wert 'false', denn 5 ist nicht kleiner als 2. Der rechte Teil entspricht einem Boolean vom Wert 'true', da der Rest von 6 / 4, 2 ist. Der gesamte Ausdruck ist 'true', wenn einer der beiden Teilausdrücke 'true' ist.\n");

		// Teilaufgabe d)
		System.out.print("d) Welchen Wert hat der Ausdruck '6 / 4'? ");
		int input4 = myScanner.nextInt();
		if (input4 == 6 / 4) {
			System.out.println("Korrekt!");
		} else {
			System.out.println("Leider falsch! Die richtige Antwort ist " + 6 / 4);
		}
		System.out.println("Bei einer Division von Integern, wird auch bei einem Ergebnis mit Nachkommastellen nicht automatisch ein Float oder Double zurückgegeben, sondern ein abgerundeter Integer.\n");

		// Teilaufgabe e)
		System.out.println("e) Welchen Wert hat der Ausdruck '6 / 4.0'? ");
		myScanner.useLocale(Locale.US);
		double input5 = myScanner.nextDouble();
		if (input5 == 6 / 4.0) {
			System.out.println("Korrekt!");
		} else {
			System.out.println("Leider falsch! Die richtige Antwort ist " + 6 / 4.0);
		}
		System.out.println("Bei einer Division von einem Integer und einer Gleitkommezahl, ist auch das Ergebnis eine Gleitkommazahl, da der Integer verlustfrei in einen Float oder Double umgewandelt werden kann.\n");

		// Teilaufgabe f)
		System.out.println("f) Welchen Wert hat der Ausdruck '(int) (10 / 4.0 + 3)'? ");
		int input6 = myScanner.nextInt();
		if (input6 == (int) (10 / 4.0 + 3)) {
			System.out.println("Korrekt!");
		} else {
			System.out.println("Leider falsch! Die richtige Antwort ist " + (int) (10 / 4.0 + 3));
		}
		System.out.println("Geschafft!");
	}
}