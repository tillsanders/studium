import java.util.Scanner;

class Aufgabe {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Bitte geben Sie eine Jahreszahl ein: ");
        int year = myScanner.nextInt();
        boolean isLeapyear = year % 4 == 0 && year % 100 != 0;
        if (isLeapyear) {
            System.out.println("Das Jahr " + year + " ist ein Schaltjahr.");
            return;
        }
        System.out.println("Das Jahr " + year + " ist kein Schaltjahr.");
    }
}