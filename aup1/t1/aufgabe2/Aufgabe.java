import java.util.Scanner;

class Aufgabe {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Bitte geben Sie eine Ganzzahl ein: ");
        int input1 = myScanner.nextInt();

        System.out.println("Bitte geben Sie eine weitere Ganzzahl ein: ");
        int input2 = myScanner.nextInt();

        int result = input1 * input2;

        System.out.println("Ergebnis der Multiplikation: " + result);
    }
}