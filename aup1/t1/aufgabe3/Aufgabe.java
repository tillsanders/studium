import java.lang.Math;

class Aufgabe {
    public static void main(String[] args) {
        System.out.println("Der Wertebereich eines Integers berechnet sich wie folgt: (2^n / -2) bis (2^n / 2) - 1.");
        System.out.println("Ein Integer hat 32 Bit, daher umfasst sein Wertebereich die ganzen Zahlen von (2^32 / -2) bis (2^8 / 2) - 1.");
        
        long lowest = (long) Math.pow(2, 32) / -2; //  -2147483648
        long highest = (long) Math.pow(2, 32) / 2 - 1; // 2147483647

        System.out.println("Das entspricht: " + lowest + " bis " + highest + ".");
        System.out.println("Eine Variable hat nun den Wert: " + highest + ".");
        
        int big = (int) highest;
        
        System.out.println("Sie wird um 1 inkrementiert. Das Resultat ist jedoch nicht " + (highest + 1) + ", sondern:");
        System.out.println(big + 1);
        System.out.println("Der Speicher ist übergelaufen. Tada.");
    }
}