import java.util.Scanner;

class Aufgabe {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Bitte geben Sie eine Temperatur in °F ein: ");
        double fahrenheit = myScanner.nextInt();
        double celsius = (5.0 / 9.0) * (fahrenheit - 32);
        double result = (
            (double) (
                (int) (celsius * 100)
            ) / 100);
        System.out.println((int) fahrenheit + " °F entsprechen " + result + " °C");
    }
}